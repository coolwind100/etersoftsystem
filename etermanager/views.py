from django.shortcuts import render
from etermanager.models import Task
import django_otp.decorators

@django_otp.decorators.otp_required()
def index(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        'tasks': tasks
    }
    return render(request, 'etermanager/index.html', context)

