from django.apps import AppConfig


class EtermanagerConfig(AppConfig):
    name = 'etermanager'
