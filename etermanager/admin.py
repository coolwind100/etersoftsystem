from django.contrib import admin
from etermanager.models import Task

admin.site.site_header = "ETERSoft manager"
admin.site.site_title = "ETERSoft manager"
admin.site.index_title = ""
admin.site.register(Task)