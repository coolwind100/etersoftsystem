from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from django_otp.admin import OTPAdminSite

class OTPAdmin(OTPAdminSite):
    pass

from django.contrib.auth.models import User
from django_otp.plugins.otp_totp.models import TOTPDevice

admin_site = OTPAdmin(name="OTPAdmin")
admin_site.register(User)
admin_site.register(TOTPDevice)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', admin_site.urls),
    path('tasks/', include('etermanager.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
